import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/latest_chapter.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class LatestMangaController extends GetxController {
  var chapterList = <LatestChapter>[].obs;

  @override
  void onInit() {
    fetchLatestManga();
    super.onInit();
  }

  void fetchLatestManga() async {
    var chapters = await MangaProvider().getLatestChapter("en", 10);
    chapterList.value = chapters!;
  }
}
