import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/data/network/me/history_response.dart';
import 'package:honyomi_mobile/data/providers/me_provider.dart';

class RecentController extends GetxController {
  final authenticationController = Get.put(AuthenticationController());
  final histories = Rx<List<History>>([]);

  @override
  void onInit() {
    if (authenticationController.isLogged.isTrue) {
      fetchHistories();
    }
    super.onInit();
  }

  fetchHistories() async {
    try {
      final histories = await MeProvider(
              accessToken: await authenticationController.getAccessToken())
          .getHistory();
      this.histories.value = histories;
    } catch (e) {
      rethrow;
    }
  }
}
