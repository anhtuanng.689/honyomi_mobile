import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class NewSeriesController extends GetxController {
  var mangaList = <Manga>[].obs;

  @override
  void onInit() {
    fetchNewSeriesManga();
    super.onInit();
  }

  void fetchNewSeriesManga() async {
    var mangas = await MangaProvider().getNewSeries();
    mangaList.value = mangas!.reversed.toList();
  }
}
