import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/network/login_request.dart';
import 'package:honyomi_mobile/data/network/login_response.dart';
import 'package:honyomi_mobile/data/providers/auth_provider.dart';
import 'package:honyomi_mobile/data/providers/network_exception.dart';

class SignInController extends GetxController {
  final signInFormKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final tokens = Rx<Tokens?>(null);

  @override
  void onClose() {
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
  }

  Future<void> signIn() async {
    if (signInFormKey.currentState!.validate()) {
      try {
        final tokens = await AuthProvider().signIn(LoginRequest(
            username: emailController.text, password: passwordController.text));
        //TODO: save token
        this.tokens.value = tokens;
      } on UnauthorisedException catch (e) {
      } catch (e) {}
    }
  }
}
