import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:honyomi_mobile/data/network/login_response.dart';
import 'package:honyomi_mobile/data/providers/auth_provider.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:honyomi_mobile/data/network/access_token_request.dart';

class AuthenticationController extends GetxController {
  static const refreshTokenKey = "refreshToken";
  static const accessTokenKey = "accessToken";

  final _getStorage = GetStorage();
  var isLogged = false.obs;
  String? refreshToken;
  String? accessToken;

  @override
  void onInit() {
    refreshToken = _getStorage.read(refreshTokenKey);
    if (refreshToken != null && !Jwt.isExpired(refreshToken!)) {
      isLogged.value = true;
    }

    super.onInit();
  }

  login(Tokens tokens) {
    _getStorage.write(refreshTokenKey, tokens.refreshToken);
    _getStorage.write(accessTokenKey, tokens.accessToken);
  }

  logout() {
    refreshToken = null;
    accessToken = null;
    isLogged.value = false;
    _getStorage.remove(refreshTokenKey);
    _getStorage.remove(accessTokenKey);
  }

  bool isValidAccessToken() {
    return isLogged.value &&
        accessToken != null &&
        !Jwt.isExpired(accessToken!);
  }

  Future<void> refreshAccessToken() async {
    if (refreshToken != null) {
      accessToken = await AuthProvider()
          .getAccessToken(AccessTokenRequest(refreshToken: refreshToken!));
    }
  }

  Future<String> getAccessToken() async {
    if (!isValidAccessToken()) {
      await refreshAccessToken();
    }

    return accessToken!;
  }
}
