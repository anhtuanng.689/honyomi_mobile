import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/library/favorite_controller.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class DetailController extends GetxController {
  var manga = Rx<Manga?>(null);

  @override
  void onInit() {
    fetchMangaDetail();
    super.onInit();
  }

  void fetchMangaDetail() async {
    final manga = await MangaProvider().getMangaDetail(Get.parameters["id"]);
    this.manga.value = manga;
  }
}
