import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/providers/me_provider.dart';

class FavoriteController extends GetxController {
  var favoriteMangas = <Manga>[].obs;

  final authenticationController = Get.put(AuthenticationController());

  @override
  void onInit() {
    getFavoriteManga();
    super.onInit();
  }

  void getFavoriteManga() async {
    try {
      favoriteMangas.value = await MeProvider(
              accessToken: await authenticationController.getAccessToken())
          .getFavorites();
    } catch (e) {
      print(e);
    }
  }

  void addFavoriteManga(String id) async {
    try {
      final manga = await MeProvider(
              accessToken: await authenticationController.getAccessToken())
          .addFavorites(id);
      favoriteMangas.value = manga;
      print(favoriteMangas.value);
    } catch (e) {
      print(e);
    }
  }

  void deleteFavoriteManga(String id) async {
    try {
      final manga = await MeProvider(
              accessToken: await authenticationController.getAccessToken())
          .deleteFavorites(id);
      favoriteMangas.value = manga;
    } catch (e) {
      print(e);
    }
  }
}
