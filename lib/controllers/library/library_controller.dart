import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class LibraryController extends GetxController {
  var popularList = <Manga>[].obs;

  @override
  void onInit() {
    getMangaPopular();
    super.onInit();
  }

  void getMangaPopular() async {
    try {
      popularList.value = (await MangaProvider().getManga());
    } catch (e) {
      print(e);
    }
  }
}
