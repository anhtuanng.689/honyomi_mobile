import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/data/providers/user_provider.dart';
import 'package:honyomi_mobile/data/models/user.dart';

class UserController extends GetxController {
  final authenticationController = Get.put(AuthenticationController());
  final user = Rx<User?>(null);

  fetchUser() async {
    try {
      final user = await UserProvider(
              accessToken: await authenticationController.getAccessToken())
          .getProfile();

      this.user.value = user;
    } catch (e) {
      rethrow;
    }
  }
}
