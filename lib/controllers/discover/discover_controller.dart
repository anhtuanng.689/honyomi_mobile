import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class DiscoverController extends GetxController {
  var isLoading = false.obs;
  var mangaList = <Manga>[].obs;
  var categories = <Category>[].obs;
  var statuses = <Status>[].obs;

  @override
  void onInit() {
    fetchManga();
    super.onInit();
  }

  void fetchManga() async {
    isLoading.value = true;
    try {
      mangaList.value = (await MangaProvider().getManga()).reversed.toList();
    } catch (e) {
      print(e);
    }
    isLoading.value = false;
  }

  void handleFilter() async {
    mangaList.value = (await MangaProvider()
            .getManga(categories: categories.value, status: statuses.value))
        .reversed
        .toList();
  }
}
