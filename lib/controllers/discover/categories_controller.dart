import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/providers/category_provider.dart';

class CategoriesController extends GetxController {
  var categories = Rx<List<Category>>([]);

  @override
  void onInit() {
    fetchCategories();
    super.onInit();
  }

  void fetchCategories() async {
    final categories = await CategoryProvider().getAllCategories();
    this.categories.value = categories;
  }
}
