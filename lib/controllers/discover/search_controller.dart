import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';
import 'package:honyomi_mobile/public/debounce.dart';

class SearchController extends GetxController {
  final debounce = Debounce(const Duration(milliseconds: 500));
  final searchBarController = TextEditingController();
  var mangaList = <Manga>[].obs;
  var isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    searchBarController.dispose();
    debounce.dispose();
    super.dispose();
  }

  void searchManga(String query) async {
    try {
      if (query == "") {
        mangaList.value = [];
      } else {
        mangaList.value = await MangaProvider().getManga(query: query);
      }
      print(mangaList);
    } catch (e) {
      print(e);
    }
  }
}
