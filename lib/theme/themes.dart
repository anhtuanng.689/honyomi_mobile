import 'package:flutter/material.dart';
import 'package:honyomi_mobile/public/colors.dart';
import 'package:honyomi_mobile/public/pallete.dart';
import 'package:honyomi_mobile/theme/theme_config.dart';

class Themes {
  final lightTheme = ThemeConfig.createTheme(
    primarySwatch: Palette.kToLight,
    brightness: Brightness.light,
    background: Colors.white.withOpacity(0.98),
    appBarColor: Colors.white,
    cardBackground: ColorConstants.lightScaffoldBackgroundColor,
    primaryText: Colors.black,
    secondaryText: Colors.black,
    accentColor: ColorConstants.secondaryAppColor,
    divider: ColorConstants.secondaryAppColor,
    buttonBackground: Colors.black,
    buttonText: ColorConstants.secondaryAppColor,
    disabled: ColorConstants.secondaryAppColor,
    error: Colors.red,
  );
  final darkTheme = ThemeConfig.createTheme(
    primarySwatch: Palette.kToDark,
    brightness: Brightness.dark,
    background: ColorConstants.darkScaffoldBackgroundColor,
    appBarColor: const Color(0xff181818),
    cardBackground: ColorConstants.darkScaffoldBackgroundColor,
    primaryText: Colors.white,
    secondaryText: Colors.white,
    accentColor: ColorConstants.secondaryDarkAppColor,
    divider: Colors.black45,
    buttonBackground: Colors.white,
    buttonText: ColorConstants.secondaryDarkAppColor,
    disabled: ColorConstants.secondaryDarkAppColor,
    error: Colors.red,
  );
}
