import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';
import 'package:honyomi_mobile/routes/app_pages.dart';
import 'package:honyomi_mobile/theme/theme_service.dart';
import 'package:honyomi_mobile/theme/themes.dart';

void main() async {
  await GetStorage.init();
  runApp(ScreenUtilInit(
    designSize: const Size(360, 690),
    builder: () => GetMaterialApp(
      debugShowCheckedModeBanner: false,
      enableLog: true,
      initialRoute: AppPages.initial,
      getPages: AppPages.routes,
      theme: Themes().lightTheme,
      darkTheme: Themes().darkTheme,
      themeMode: ThemeService().getThemeMode(),
    ),
  ));
}
