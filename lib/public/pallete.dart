import 'package:flutter/material.dart';

class Palette {
  static const MaterialColor kToLight = MaterialColor(
    0xfff4f4f4,
    // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    <int, Color>{
      50: Color(0xffdcdcdc), //10%
      100: Color(0xffc3c3c3), //20%
      200: Color(0xffababab), //30%
      300: Color(0xff929292), //40%
      400: Color(0xff7a7a7a), //50%
      500: Color(0xff626262), //60%
      600: Color(0xff494949), //70%
      700: Color(0xff313131), //80%
      800: Color(0xff181818), //90%
      900: Color(0xff000000), //100%
    },
  );
  static const MaterialColor kToDark = MaterialColor(
    0xff1a1a1a,
    // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    <int, Color>{
      50: Color(0xff171717), //10%
      100: Color(0xff151515), //20%
      200: Color(0xff121212), //30%
      300: Color(0xff101010), //40%
      400: Color(0xff0d0d0d), //50%
      500: Color(0xff0a0a0a), //60%
      600: Color(0xff080808), //70%
      700: Color(0xff050505), //80%
      800: Color(0xff030303), //90%
      900: Color(0xff000000), //100%
    },
  );
}
