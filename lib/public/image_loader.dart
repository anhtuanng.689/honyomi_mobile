import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:honyomi_mobile/widgets/spin_kit.dart';

class ImageLoader {
  static CachedNetworkImage loadDefault(
      {required String url, BoxFit fit = BoxFit.cover}) {
    return CachedNetworkImage(
      imageUrl: url,
      fit: fit,
      placeholder: (context, url) => const Center(child: SpinKit()),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }

  static CachedNetworkImage loadAvatar(
      {required String url, BoxFit fit = BoxFit.fill}) {
    return CachedNetworkImage(
      imageUrl: url,
      imageBuilder: (context, imageProvider) => CircleAvatar(
        backgroundImage: imageProvider,
      ),
      fit: fit,
      placeholder: (context, url) => const Center(child: SpinKit()),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }
}
