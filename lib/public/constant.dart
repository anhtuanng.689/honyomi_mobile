import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

const baseApi = "https://honyomi.d3s34.me/api";
const baseMeApi = "$baseApi/me";
const baseAuthApi = "$baseApi/auth";
const baseMangaApi = "$baseApi/manga";
const baseCategoryApi = "$baseApi/categories";
var width = Get.width;
var height = Get.height;

TextStyle heading2 = TextStyle(
  fontSize: 24.sp,
  fontWeight: FontWeight.w700,
);

TextStyle heading5 = TextStyle(
  fontSize: 18.sp,
  fontWeight: FontWeight.w600,
);

TextStyle heading6 = TextStyle(
  fontSize: 16.sp,
  fontWeight: FontWeight.w600,
);

TextStyle regular16pt = TextStyle(
  fontSize: 16.sp,
  fontWeight: FontWeight.w400,
);
