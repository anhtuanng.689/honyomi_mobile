import 'package:flutter/material.dart';

class ColorConstants {
  static Color lightScaffoldBackgroundColor = const Color(0xFFF4f4f4);
  static Color darkScaffoldBackgroundColor = const Color(0xff1a1a1a);
  static Color secondaryAppColor = const Color(0xffe3e3e3);
  static Color secondaryDarkAppColor = const Color(0xFF202020);
  static Color appBarDarkColor = const Color(0xFF272727);
  static Color tipColor = const Color(0xFFB6B6B6);
  static Color lightGray = const Color(0xFFF6F6F6);
  static Color darkGray = const Color(0xFF9F9F9F);
  static Color black = const Color(0xFF000000);
  static Color white = const Color(0xFFFFFFFF);
}
