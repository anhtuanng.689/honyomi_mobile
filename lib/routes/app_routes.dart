part of 'app_pages.dart';

abstract class Routes {
  static const root = '/root';
  static const signIn = '/sign-in';
  static const signUp = '/sign-up';
  static const home = '/';
  static const discover = '/discover';
  static const library = "/library";
  static const detail = '/detail/:id';
  static const reader = '/detail/:id/:number';
  static const user = '/user';
  static const search = '/search';
}
