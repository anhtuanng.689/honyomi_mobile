import 'package:get/get.dart';
import 'package:honyomi_mobile/app.dart';
import 'package:honyomi_mobile/pages/auth/sign_in/sign_in.dart';
import 'package:honyomi_mobile/pages/auth/sign_up/sign_up.dart';
import 'package:honyomi_mobile/pages/detail/manga_detail.dart';
import 'package:honyomi_mobile/pages/discover/discover.dart';
import 'package:honyomi_mobile/pages/discover/search/search.dart';
import 'package:honyomi_mobile/pages/library/library.dart';
import 'package:honyomi_mobile/pages/reader/manga_reader.dart';
import 'package:honyomi_mobile/pages/user/user_page.dart';

part 'app_routes.dart';

class AppPages {
  static const initial = Routes.home;

  static final routes = [
    GetPage(
      name: Routes.home,
      page: () => const App(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.detail,
      page: () => MangaDetail(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.reader,
      page: () => MangaReader(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.discover,
      page: () => Discover(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.signIn,
      page: () => SignIn(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.signUp,
      page: () => SignUp(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.user,
      page: () => UserPage(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.search,
      page: () => Search(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: Routes.library,
      page: () => Library(),
      transition: Transition.fadeIn,
    ),
  ];
}
