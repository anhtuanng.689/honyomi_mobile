import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/controllers/user/user_controller.dart';
import 'package:honyomi_mobile/data/models/user.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

class Profile extends StatelessWidget {
  Profile({Key? key}) : super(key: key);

  final authenticationController = Get.put(AuthenticationController());
  final userController = Get.put(UserController());

  Widget profile(String avatarUrl, String username) {
    return Column(
      children: [
        SizedBox(
          width: 10.w,
        ),
        Center(
          child: SizedBox(
            width: 0.4.sw,
            height: 0.3.sh,
            child: ImageLoader.loadAvatar(url: avatarUrl),
          ),
        ),
        SizedBox(
          width: 5.w,
        ),
        Text(
          username,
          style: heading5,
        ),
      ],
    );
  }

  Widget loggedProfile(User user) {
    return profile(user.avatarUrl, user.username);
  }

  Widget guestProfile() {
    return profile(
        'https://seeklogo.com/images/M/Manga-logo-000566115A-seeklogo.com.png',
        "");
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (authenticationController.isLogged.value == true) {
        if (userController.user.value != null) {
          return loggedProfile(userController.user.value!);
        } else {
          userController.fetchUser();
          return guestProfile();
        }
      }

      return guestProfile();
    });
  }
}
