import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/public/colors.dart';
import 'package:honyomi_mobile/routes/app_pages.dart';
import 'package:honyomi_mobile/widgets/custom_primary_button.dart';

class Menu extends StatelessWidget {
  Menu({Key? key}) : super(key: key);

  final AuthenticationController authenticationController =
      Get.put(AuthenticationController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        children: [
          SizedBox(
            height: 15.h,
          ),
          CustomPrimaryButton(
            onTap: () {},
            textValue: 'Account',
            height: 50.h,
            icon: Icons.account_circle,
            buttonColor: ColorConstants.lightScaffoldBackgroundColor,
          ),
          SizedBox(
            height: 15.h,
          ),
          CustomPrimaryButton(
            onTap: () {},
            textValue: 'Setting',
            height: 50.h,
            icon: Icons.settings,
            buttonColor: ColorConstants.lightScaffoldBackgroundColor,
          ),
          SizedBox(
            height: 15.h,
          ),
          CustomPrimaryButton(
            onTap: () {},
            textValue: 'About',
            height: 50.h,
            icon: Icons.info,
            buttonColor: ColorConstants.lightScaffoldBackgroundColor,
          ),
          SizedBox(
            height: 15.h,
          ),
          authenticationController.isLogged.value == true
              ? CustomPrimaryButton(
                  height: 50.h,
                  textValue: "Sign out",
                  textColor: Colors.white,
                  buttonColor: Colors.black,
                  onTap: () {
                    authenticationController.logout();
                    Get.offAndToNamed(Routes.home);
                  })
              : CustomPrimaryButton(
                  height: 50.h,
                  textValue: "Sign in",
                  textColor: Colors.white,
                  buttonColor: Colors.black,
                  onTap: () {
                    Get.offAndToNamed(Routes.signIn);
                  }),
          SizedBox(
            height: 15.h,
          ),
        ],
      );
    });
  }
}
