import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/auth/sign_up_controller.dart';
import 'package:honyomi_mobile/public/colors.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:honyomi_mobile/widgets/custom_primary_button.dart';

class SignUp extends StatelessWidget {
  SignUp({Key? key}) : super(key: key);

  // bool passwordVisible = false;
  // bool passwordConfrimationVisible = false;
  // void togglePassword() {
  //   setState(() {
  //     passwordVisible = !passwordVisible;
  //   });
  // }

  final SignUpController signUpController = Get.put(SignUpController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 40.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sign up',
                    style: heading2,
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Image.asset(
                    "assets/images/accent.png",
                    color: Colors.black,
                    scale: 4,
                  ),
                ],
              ),
              SizedBox(
                height: 40.h,
              ),
              Form(
                key: signUpController.signUpFormKey,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextFormField(
                        controller: signUpController.usernameController,
                        decoration: InputDecoration(
                          hintText: 'Username',
                          hintStyle:
                              heading6.copyWith(color: ColorConstants.darkGray),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextFormField(
                        controller: signUpController.emailController,
                        decoration: InputDecoration(
                          hintText: 'Email',
                          hintStyle:
                              heading6.copyWith(color: ColorConstants.darkGray),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextFormField(
                        controller: signUpController.passwordController,
                        // obscureText: !passwordVisible,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          hintStyle:
                              heading6.copyWith(color: ColorConstants.darkGray),
                          // suffixIcon: IconButton(
                          //   color: textGrey,
                          //   splashRadius: 1,
                          //   icon: Icon(passwordVisible
                          //       ? Icons.visibility_outlined
                          //       : Icons.visibility_off_outlined),
                          //   onPressed: togglePassword,
                          // ),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   height: 25.h,
                    // ),
                    // Container(
                    //   decoration: BoxDecoration(
                    //     color: Colors.white,
                    //     borderRadius: BorderRadius.circular(15),
                    //   ),
                    //   child: TextFormField(
                    //     // obscureText: !passwordConfrimationVisible,
                    //     decoration: InputDecoration(
                    //       hintText: 'Password Confirmation',
                    //       hintStyle:
                    //           heading6.copyWith(color: ColorConstants.darkGray),
                    //       // suffixIcon: IconButton(
                    //       //   color: textGrey,
                    //       //   splashRadius: 1,
                    //       //   icon: Icon(passwordConfrimationVisible
                    //       //       ? Icons.visibility_outlined
                    //       //       : Icons.visibility_off_outlined),
                    //       //   onPressed: () {
                    //       //     setState(() {
                    //       //       passwordConfrimationVisible =
                    //       //           !passwordConfrimationVisible;
                    //       //     });
                    //       //   },
                    //       // ),
                    //       border: const OutlineInputBorder(
                    //         borderSide: BorderSide.none,
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
              SizedBox(
                height: 25.h,
              ),
              CustomPrimaryButton(
                height: 50.h,
                textValue: "Sign up",
                textColor: Colors.white,
                buttonColor: Colors.black,
                onTap: () {
                  print("Sign up");
                  signUpController.signUp();
                },
              ),
              SizedBox(
                height: 40.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Already have an account? ",
                    style: regular16pt.copyWith(color: ColorConstants.darkGray),
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.offAndToNamed("/sign-in");
                    },
                    child: Text(
                      'Sign in',
                      style: regular16pt,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
