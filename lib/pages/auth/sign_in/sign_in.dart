import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/controllers/auth/sign_in_controller.dart';
import 'package:honyomi_mobile/pages/dashboard.dart';
import 'package:honyomi_mobile/public/colors.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:honyomi_mobile/routes/app_pages.dart';
import 'package:honyomi_mobile/widgets/custom_primary_button.dart';

class SignIn extends StatelessWidget {
  SignIn({Key? key}) : super(key: key);

/*  bool passwordVisible = false;
  void togglePassword() {
      passwordVisible = !passwordVisible;
  }*/

  final SignInController signInController = Get.put(SignInController());
  final AuthenticationController authenticationController =
      Get.put(AuthenticationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 40.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sign in',
                    style: heading2,
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Image.asset(
                    "assets/images/accent.png",
                    color: Colors.black,
                    scale: 4,
                  ),
                ],
              ),
              SizedBox(
                height: 40.h,
              ),
              Form(
                key: signInController.signInFormKey,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextFormField(
                        controller: signInController.emailController,
                        decoration: InputDecoration(
                          hintText: 'Email',
                          hintStyle:
                              heading6.copyWith(color: ColorConstants.darkGray),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 32.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextFormField(
                        controller: signInController.passwordController,
                        // obscureText: !passwordVisible,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          hintStyle:
                              heading6.copyWith(color: ColorConstants.darkGray),
/*                          suffixIcon: IconButton(
                            color: textGrey,
                            splashRadius: 1,
                            icon: Icon(passwordVisible
                                ? Icons.visibility_outlined
                                : Icons.visibility_off_outlined),
                            onPressed: togglePassword,
                          ),*/
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 32.h,
              ),
              CustomPrimaryButton(
                height: 50.h,
                textValue: "Sign in",
                textColor: Colors.white,
                buttonColor: Colors.black,
                onTap: () {
                  signInController.signIn().then((value) {
                    if (signInController.tokens.value == null) {
                      Get.snackbar('Sign in', 'Invalid email or password',
                          snackPosition: SnackPosition.BOTTOM);
                    } else {
                      authenticationController
                          .login(signInController.tokens.value!);
                      Get.offAndToNamed(Routes.home);
                    }
                  });
                },
              ),
/*              SizedBox(
                height: 24.h,
              ),
              Center(
                child: Text(
                  'OR',
                  style: heading6.copyWith(color: ColorConstants.darkGray),
                ),
              ),
              SizedBox(
                height: 24.h,
              ),
              CustomPrimaryButton(
                height: 50.h,
                textValue: "Sign in with Google",
                onTap: () {
                  print("Sign in with Google");
                },
              ),*/
              SizedBox(
                height: 50.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Don't have an account? ",
                    style: regular16pt.copyWith(color: ColorConstants.darkGray),
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.offAndToNamed(Routes.signUp);
                    },
                    child: Text(
                      'Sign up',
                      style: regular16pt,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
