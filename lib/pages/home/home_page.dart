import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:honyomi_mobile/pages/home/recent_reading/recent_reading.dart';
import 'package:honyomi_mobile/public/image_loader.dart';
import 'package:honyomi_mobile/theme/theme_service.dart';
import 'package:honyomi_mobile/widgets/bottom_nav_bar.dart';
import 'package:honyomi_mobile/widgets/carousel.dart';
import 'package:honyomi_mobile/widgets/manga_icon_button.dart';

import 'latest_update/latest_update.dart';
import 'new_series/new_series.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0.075.sh,
        title: const Text("Honyomi"),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 16.w),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    ThemeService().changeThemeMode();
                  },
                  child: MangaIconButton(
                    icon: Theme.of(context).brightness == Brightness.dark
                        ? Icons.dark_mode
                        : Icons.light_mode,
                  ),
                ),
                SizedBox(width: 10.w),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Carousel(),
            Padding(
              padding: EdgeInsets.only(left: 10.w, right: 10.w, bottom: 10.h),
              child: Column(
                children: [
                  // TODO: fetch history api
                  RecentReading(),
                  LatestUpdate(),
                  NewSeries(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
