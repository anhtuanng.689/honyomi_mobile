import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/controllers/home/recent_controller.dart';
import 'package:honyomi_mobile/pages/detail/manga_detail.dart';
import 'package:honyomi_mobile/pages/reader/manga_reader.dart';
import 'package:honyomi_mobile/widgets/recent_manga_card.dart';

class RecentReading extends StatelessWidget {
  RecentReading({Key? key}) : super(key: key);

  final AuthenticationController authenticationController =
      Get.put(AuthenticationController());

  final RecentController recentController = Get.put(RecentController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return !authenticationController.isLogged.isTrue ||
              recentController.histories.value.isEmpty
          ? Container()
          : Padding(
              padding: EdgeInsets.only(top: 10.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Recent Reading',
                    style: TextStyle(
                      fontSize: 20.sp,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  SizedBox(
                    height: 0.25.sh,
                    child: ListView.separated(
                      scrollDirection: Axis.horizontal,
                      itemCount: recentController.histories.value.length,
                      separatorBuilder: (context, _) => SizedBox(width: 10.w),
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () => Get.toNamed(
                            "/detail/${recentController.histories.value[index].chapter.manga.id}"),
                        child: RecentMangaCard(
                            history: recentController.histories.value[index]),
                      ),
                    ),
                  ),
                ],
              ),
            );
    });
  }
}
