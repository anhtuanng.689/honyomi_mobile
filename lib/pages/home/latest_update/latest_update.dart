import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/home/latest_manga_controller.dart';
import 'package:honyomi_mobile/widgets/manga_chapter_card.dart';

class LatestUpdate extends StatelessWidget {
  LatestUpdate({Key? key}) : super(key: key);

  final LatestMangaController latestChapterController =
      Get.put(LatestMangaController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Latest Update',
            style: TextStyle(
              fontSize: 20.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(
            height: 10.h,
          ),
          SizedBox(
            height: 0.4.sh,
            child: Obx(
              () => ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: latestChapterController.chapterList.length,
                separatorBuilder: (context, _) => SizedBox(width: 10.w),
                itemBuilder: (context, index) => MangaChapterCard(
                  latestChapter: latestChapterController.chapterList[index],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
