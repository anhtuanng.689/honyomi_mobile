import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/home/new_series_controller.dart';
import 'package:honyomi_mobile/widgets/manga_categories_card.dart';

class NewSeries extends StatelessWidget {
  NewSeries({Key? key}) : super(key: key);

  final NewSeriesController newSeriesController =
      Get.put(NewSeriesController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'New Series',
            style: TextStyle(
              fontSize: 20.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(
            height: 10.h,
          ),
          SizedBox(
            height: 0.35.sh,
            child: Obx(() => ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: newSeriesController.mangaList.length,
                  separatorBuilder: (context, _) => SizedBox(width: 10.w),
                  itemBuilder: (context, index) => MangaCategoriesCard(
                    manga: newSeriesController.mangaList[index],
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
