import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:honyomi_mobile/pages/discover/search/search_bar.dart';
import 'manga_grid/manga_grid.dart';

class Discover extends StatelessWidget {
  Discover({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
          child: Column(
            children: [
              SearchBar(),
              MangaGrid(),
            ],
          ),
        ),
      ),
    );
  }
}
