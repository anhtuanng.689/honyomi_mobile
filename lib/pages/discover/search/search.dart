import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/discover/search_controller.dart';
import 'package:honyomi_mobile/public/colors.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:honyomi_mobile/widgets/manga_card.dart';
import 'package:honyomi_mobile/widgets/spin_kit.dart';

class Search extends StatelessWidget {
  Search({Key? key}) : super(key: key);
  TextEditingController textController = TextEditingController();

  final SearchController searchController = Get.put(SearchController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
            child: Column(
              children: [
                Container(
                  height: 0.12.sw,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Theme.of(context).brightness == Brightness.dark
                        ? ColorConstants.appBarDarkColor
                        : ColorConstants.lightScaffoldBackgroundColor,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: TextFormField(
                    autofocus: true,
                    controller: searchController.searchBarController,
                    textAlign: TextAlign.start,
                    textAlignVertical: TextAlignVertical.center,
                    decoration: InputDecoration(
                      filled: true,
                      hintText: 'Search',
                      hintStyle:
                          heading6.copyWith(color: ColorConstants.darkGray),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.all(
                          Radius.circular(15.0),
                        ),
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.clear,
                          color: Colors.black,
                          size: 20.w,
                        ),
                        onPressed: () {
                          searchController.searchBarController.clear();
                        },
                      ),
                    ),
                    onChanged: (text) {
                      searchController.debounce(() {
                        searchController.searchManga(text);
                      });
                    },
                    key: key,
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                buildListManga(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildListManga() {
    return Obx(
      () => searchController.mangaList.isEmpty
          ? Center(
              child: Text(
                "No result found",
                style: heading6,
              ),
            )
          : Column(
              children: [
                Center(
                  child: Text(
                    searchController.mangaList.length == 1
                        ? "Found 1 result"
                        : "Found ${searchController.mangaList.length} results",
                    style: heading6,
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                GridView.builder(
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  scrollDirection: Axis.vertical,
                  itemCount: searchController.mangaList.length,
                  itemBuilder: (context, index) => Padding(
                    padding: EdgeInsets.all(5.w),
                    child: MangaCard(
                      manga: searchController.mangaList[index],
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
