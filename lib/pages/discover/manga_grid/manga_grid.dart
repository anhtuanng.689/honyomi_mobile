import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/discover/categories_controller.dart';
import 'package:honyomi_mobile/controllers/discover/discover_controller.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/widgets/categories_card.dart';
import 'package:honyomi_mobile/widgets/manga_card.dart';
import 'package:honyomi_mobile/widgets/spin_kit.dart';
import 'package:honyomi_mobile/widgets/statuses_card.dart';

class MangaGrid extends StatelessWidget {
  MangaGrid({Key? key}) : super(key: key);

  final DiscoverController discoverController = Get.put(DiscoverController());
  final CategoriesController categoriesController =
      Get.put(CategoriesController());

  Widget showCategories(List<Category> categories) {
    return CategoriesCard(
      categories: categories,
      title: "category",
    );
  }

  Widget showStatus(List<Status> statuses) {
    return StatusesCard(
      statuses: statuses,
      title: "status",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: EdgeInsets.only(top: 10.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Discover',
              style: TextStyle(
                fontSize: 20.sp,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            showCategories(categoriesController.categories.value),
            showStatus(Status.values),
            SizedBox(
              height: 10.h,
            ),
            discoverController.isLoading.value
                ? const Center(child: SpinKit())
                : SizedBox(
                    child: GridView.builder(
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2),
                      scrollDirection: Axis.vertical,
                      itemCount: discoverController.mangaList.length,
                      itemBuilder: (context, index) => Padding(
                        padding: EdgeInsets.all(5.w),
                        child: MangaCard(
                          manga: discoverController.mangaList[index],
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
