import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/public/colors.dart';
import 'package:honyomi_mobile/widgets/chapter_card.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class MangaChapter extends StatelessWidget {
  const MangaChapter({Key? key, required this.manga}) : super(key: key);
  final Manga manga;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                'Chapters',
                style: TextStyle(
                  fontSize: 20.sp,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text(
                ' (${manga.chapters!.length})',
                style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w500,
                  color: Theme.of(context).brightness == Brightness.dark
                      ? ColorConstants.tipColor
                      : Colors.grey.shade900,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.h,
          ),
          SizedBox(
            child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount:
                  manga.chapters!.length < 5 ? manga.chapters!.length : 5,
              separatorBuilder: (context, _) => SizedBox(height: 10.h),
              itemBuilder: (context, index) => GestureDetector(
                onTap: () => Get.toNamed("/detail/${manga.id}/${index + 1}",
                    arguments: [manga, index]),
                child: ChapterCard(
                  chapter: manga.chapters![index],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10.h,
          ),
          Center(
            child: ElevatedButton(
              onPressed: () {
                showMaterialModalBottomSheet(
                  bounce: true,
                  context: context,
                  builder: (context) => ListView.separated(
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    padding: EdgeInsets.all(10.h),
                    itemCount: manga.chapters!.length,
                    separatorBuilder: (context, _) => SizedBox(height: 10.h),
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () => Get.toNamed(
                          "/detail/${manga.id}/${index + 1}",
                          arguments: [manga, index]),
                      child: ChapterCard(
                        chapter: manga.chapters![index],
                      ),
                    ),
                  ),
                );
              },
              child: Text(
                'See all ${manga.chapters!.length} chapters',
                style: TextStyle(
                  fontSize: 16.sp,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
