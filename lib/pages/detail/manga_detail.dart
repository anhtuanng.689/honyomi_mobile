import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/detail/detail_controller.dart';
import 'package:honyomi_mobile/controllers/library/favorite_controller.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/widgets/manga_icon_button.dart';
import 'package:honyomi_mobile/widgets/spin_kit.dart';

import 'manga_chapter/manga_chapter.dart';
import 'manga_image/manga_image.dart';
import 'manga_summary/manga_summary.dart';

class MangaDetail extends StatelessWidget {
  MangaDetail({Key? key}) : super(key: key);

  final DetailController detailController = Get.put(DetailController());
  final FavoriteController favoriteController = Get.put(FavoriteController());

  void addFavorite(Manga manga) {
    favoriteController.addFavoriteManga(manga.id);
  }

  void removeFavorite(Manga manga) {
    favoriteController.deleteFavoriteManga(manga.id);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      final manga = detailController.manga.value;
      final isFavorite = favoriteController.favoriteMangas.contains(manga);

      return Scaffold(
        appBar: AppBar(
          toolbarHeight: 0.075.sh,
          title: Text(manga != null ? manga.title : "Manga title"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Padding(
            padding: EdgeInsets.only(left: 10.w, right: 10.w, bottom: 10.h),
            child: manga != null
                ? Column(
                    children: [
                      MangaImage(
                        manga: manga,
                        isFavorite: isFavorite,
                        addFavorite: () => addFavorite(manga),
                        removeFavorite: () => removeFavorite(manga),
                      ),
                      MangaSummary(manga: manga),
                      MangaChapter(manga: manga),
                    ],
                  )
                : Container(),
          ),
        ),
      );
    });
  }
}
