import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/widgets/categories_card.dart';

class MangaSummary extends StatelessWidget {
  const MangaSummary({Key? key, required this.manga}) : super(key: key);

  final Manga manga;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Categories',
            style: TextStyle(
              fontSize: 20.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
          CategoriesCard(
            categories: manga.categories!,
            title: "detail",
          ),
          SizedBox(
            height: 10.h,
          ),
          Text(
            'Authors',
            style: TextStyle(
              fontSize: 20.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(
            height: 10.h,
          ),
          Text(
            manga.authors!
                .map((e) => e.name)
                .toList()
                .toString()
                .replaceAll('[', '')
                .replaceAll(']', ''),
            style: TextStyle(
              fontSize: 14.sp,
              fontWeight: FontWeight.w400,
            ),
            softWrap: true,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 10.h,
          ),
          ExpandablePanel(
            theme: ExpandableThemeData(
              iconColor: Theme.of(context).brightness == Brightness.dark
                  ? Colors.white
                  : Colors.black,
            ),
            header: Padding(
              padding: EdgeInsets.only(top: 5.0.h),
              child: Text(
                'Summary',
                style: TextStyle(
                  fontSize: 20.sp,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            collapsed: Text(
              manga.synopsis,
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w400,
              ),
              softWrap: true,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            expanded: Text(
              manga.synopsis,
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w400,
              ),
              softWrap: true,
            ),
          ),
        ],
      ),
    );
  }
}
