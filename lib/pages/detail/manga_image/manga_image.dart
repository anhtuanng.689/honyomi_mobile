import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/detail/detail_controller.dart';
import 'package:honyomi_mobile/controllers/library/favorite_controller.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

class MangaImage extends StatelessWidget {
  MangaImage(
      {Key? key,
      required this.manga,
      required this.isFavorite,
      required this.addFavorite,
      required this.removeFavorite})
      : super(key: key);

  final Manga manga;
  final bool isFavorite;
  final VoidCallback addFavorite;
  final VoidCallback removeFavorite;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10.h),
      child: Center(
        child: Column(
          children: [
            SizedBox(
              height: 10.h,
            ),
            Stack(children: [
              SizedBox(
                height: 0.4.sh,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.r),
                  child: ImageLoader.loadDefault(
                      url: manga.coverImage.original.url),
                ),
              ),
              Positioned(
                bottom: 1.h,
                right: -15.w,
                child: RawMaterialButton(
                  onPressed: () {
                    if (isFavorite) {
                      removeFavorite();
                    } else {
                      addFavorite();
                    }
                  },
                  fillColor: Theme.of(context).brightness == Brightness.dark
                      ? Colors.white
                      : Colors.black,
                  child: Icon(
                    isFavorite
                        ? Icons.favorite
                        : Icons.favorite_border_outlined,
                    size: 20.w,
                    color: Theme.of(context).brightness == Brightness.dark
                        ? Colors.black
                        : Colors.white,
                  ),
                  padding: EdgeInsets.all(5.w),
                  shape: const CircleBorder(),
                ),
              )
            ]),
            SizedBox(
              height: 10.h,
            ),
            Text(
              'Status: ${describeEnum(manga.status).capitalizeFirst}',
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
          ],
        ),
      ),
    );
  }
}
