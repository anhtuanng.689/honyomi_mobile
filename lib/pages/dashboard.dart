import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/dashboard_controller.dart';
import 'package:honyomi_mobile/pages/home/home_page.dart';
import 'package:honyomi_mobile/widgets/bottom_nav_bar.dart';
import 'package:honyomi_mobile/pages/user/user_page.dart';

import 'discover/discover.dart';
import 'library/library.dart';

class Dashboard extends StatelessWidget {
  Dashboard({Key? key}) : super(key: key);

  final DashboardController dashboardController =
      Get.put(DashboardController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(
          () => IndexedStack(
            index: dashboardController.tabIndex.value,
            children: [
              const HomePage(),
              Discover(),
              Library(),
              UserPage(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Navbar(),
    );
  }
}
