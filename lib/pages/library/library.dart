import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:honyomi_mobile/pages/library/popular/popular.dart';
import 'package:honyomi_mobile/pages/library/recent/recent.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

import 'favorite/favorite.dart';

class Library extends StatelessWidget {
  Library({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Library",
            style: TextStyle(
              fontSize: 20.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
          elevation: 0,
          bottom: TabBar(
            indicatorColor: Colors.green,
            labelStyle: heading6,
            tabs: ["Popular", "Favorite"].map((e) => Tab(text: e)).toList(),
            indicator: DotIndicator(
              distanceFromCenter: 20,
              radius: 3,
              paintingStyle: PaintingStyle.fill,
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
          child: TabBarView(children: [
            PopularManga(),
            // RecentManga(),
            FavoriteManga(),
          ]),
        ),
      ),
    );
  }
}
