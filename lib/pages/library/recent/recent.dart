import 'package:flutter/material.dart';

class RecentManga extends StatelessWidget {
  const RecentManga({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          Icon(Icons.apps),
          Icon(Icons.movie),
          Icon(Icons.games),
        ],
      ),
    );
  }
}
