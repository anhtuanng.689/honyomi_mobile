import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/library/library_controller.dart';
import 'package:honyomi_mobile/widgets/popular_card.dart';

class PopularManga extends StatelessWidget {
  PopularManga({Key? key}) : super(key: key);

  final LibraryController libraryController = Get.put(LibraryController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ListView.separated(
        scrollDirection: Axis.vertical,
        itemCount: libraryController.popularList.length,
        separatorBuilder: (context, _) => SizedBox(height: 10.h),
        itemBuilder: (context, index) => PopularCard(
          manga: libraryController.popularList[index],
          index: index + 1,
        ),
      ),
    );
  }
}
