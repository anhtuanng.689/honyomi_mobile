import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/library/favorite_controller.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:honyomi_mobile/widgets/manga_card.dart';

class FavoriteManga extends StatelessWidget {
  FavoriteManga({Key? key}) : super(key: key);

  final FavoriteController favoriteController = Get.put(FavoriteController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: favoriteController.favoriteMangas.isEmpty
            ? Center(
                child: Text(
                  "No favorites to show",
                  style: heading6,
                ),
              )
            : GridView.builder(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                scrollDirection: Axis.vertical,
                itemCount: favoriteController.favoriteMangas.length,
                itemBuilder: (context, index) => Padding(
                  padding: EdgeInsets.all(5.w),
                  child: MangaCard(
                    manga: favoriteController.favoriteMangas[index],
                  ),
                ),
              ),
      );
    });
  }
}
