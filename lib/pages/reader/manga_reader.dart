import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/widgets/chapter_reader.dart';

class MangaReader extends StatelessWidget {
  MangaReader({Key? key}) : super(key: key);

  Manga manga = Get.arguments[0];
  int index = Get.arguments[1];

  @override
  Widget build(BuildContext context) {
    return ChapterReader(
      listChapterUrl: manga.chapters![index].pages,
    );
  }
}
