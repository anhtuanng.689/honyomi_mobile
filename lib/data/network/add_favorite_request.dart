import 'package:json_annotation/json_annotation.dart';

part 'add_favorite_request.g.dart';

@JsonSerializable()
class AddFavoriteRequest {
  late String manga;

  AddFavoriteRequest({required this.manga});

  factory AddFavoriteRequest.fromJson(Map<String, dynamic> json) =>
      _$AddFavoriteRequestFromJson(json);

  Map<String, dynamic> toJson() => _$AddFavoriteRequestToJson(this);
}
