
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:json_annotation/json_annotation.dart';

part 'list_manga_response.g.dart';

@JsonSerializable()
class ListMangaResponse {
  late List<Manga> data;

  ListMangaResponse({required this.data});

  factory ListMangaResponse.fromJson(Map<String, dynamic> json) => _$ListMangaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ListMangaResponseToJson(this);
}