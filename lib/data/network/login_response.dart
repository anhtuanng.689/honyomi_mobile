import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  late Tokens data;
  LoginResponse({required this.data});

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);
}

@JsonSerializable()
class Tokens {
  late String refreshToken;
  late String accessToken;

  Tokens({required this.refreshToken, required this.accessToken});

  factory Tokens.fromJson(Map<String, dynamic> json) => _$TokensFromJson(json);
}
