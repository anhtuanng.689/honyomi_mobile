// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_series_manga.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewSeriesManga _$NewSeriesMangaFromJson(Map<String, dynamic> json) =>
    NewSeriesManga(
      (json['data'] as List<dynamic>)
          .map((e) => Manga.fromJson(e as Map<String, dynamic>))
          .toList(),
      PagingMeta.fromJson(json['meta'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$NewSeriesMangaToJson(NewSeriesManga instance) =>
    <String, dynamic>{
      'data': instance.data,
      'meta': instance.meta,
    };
