import 'package:json_annotation/json_annotation.dart';

part 'access_token_request.g.dart';

@JsonSerializable()
class AccessTokenRequest {
  late String refreshToken;

  AccessTokenRequest({required this.refreshToken});

  Map<String, dynamic> toJson() => _$AccessTokenRequestToJson(this);
}
