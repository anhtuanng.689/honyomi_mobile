import 'package:honyomi_mobile/data/models/latest_chapter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'latest_chapter_response.g.dart';

@JsonSerializable()
class LatestMangaResponse {
  late List<LatestChapter> data;

  LatestMangaResponse({required this.data});

  factory LatestMangaResponse.fromJson(Map<String, dynamic> json) =>
      _$LatestMangaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LatestMangaResponseToJson(this);
}
