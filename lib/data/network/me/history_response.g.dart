// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HistoryResponse _$HistoryResponseFromJson(Map<String, dynamic> json) =>
    HistoryResponse(
      data: (json['data'] as List<dynamic>)
          .map((e) => History.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HistoryResponseToJson(HistoryResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

History _$HistoryFromJson(Map<String, dynamic> json) => History(
      userId: json['userId'] as String,
      createdAt: json['createdAt'] as String,
      chapter: ChapterHistory.fromJson(json['chapter'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HistoryToJson(History instance) => <String, dynamic>{
      'userId': instance.userId,
      'createdAt': instance.createdAt,
      'chapter': instance.chapter,
    };

ChapterHistory _$ChapterHistoryFromJson(Map<String, dynamic> json) =>
    ChapterHistory(
      id: json['id'] as String,
      title: json['title'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
      number: json['number'] as String,
      manga: MangaHistory.fromJson(json['manga'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChapterHistoryToJson(ChapterHistory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'thumbnailUrl': instance.thumbnailUrl,
      'number': instance.number,
      'manga': instance.manga,
    };

MangaHistory _$MangaHistoryFromJson(Map<String, dynamic> json) => MangaHistory(
      id: json['id'] as String,
      title: json['title'] as String,
      romajiTitle: json['romajiTitle'] as String,
      coverImage:
          CoverImage.fromJson(json['coverImage'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MangaHistoryToJson(MangaHistory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'romajiTitle': instance.romajiTitle,
      'coverImage': instance.coverImage,
    };
