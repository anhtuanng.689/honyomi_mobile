import 'package:honyomi_mobile/data/models/cover_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'history_response.g.dart';

@JsonSerializable()
class HistoryResponse {
  late List<History> data;

  HistoryResponse({required this.data});

  factory HistoryResponse.fromJson(Map<String, dynamic> json) =>
      _$HistoryResponseFromJson(json);
}

@JsonSerializable()
class History {
  late String userId;
  late String createdAt;
  late ChapterHistory chapter;

  History(
      {required this.userId, required this.createdAt, required this.chapter});

  factory History.fromJson(Map<String, dynamic> json) =>
      _$HistoryFromJson(json);
}

@JsonSerializable()
class ChapterHistory {
  late String id;
  late String title;
  late String thumbnailUrl;
  late String number;
  late MangaHistory manga;

  ChapterHistory(
      {required this.id,
      required this.title,
      required this.thumbnailUrl,
      required this.number,
      required this.manga});

  factory ChapterHistory.fromJson(Map<String, dynamic> json) =>
      _$ChapterHistoryFromJson(json);
}

@JsonSerializable()
class MangaHistory {
  late String id;
  late String title;
  late String romajiTitle;
  late CoverImage coverImage;

  MangaHistory(
      {required this.id,
      required this.title,
      required this.romajiTitle,
      required this.coverImage});

  factory MangaHistory.fromJson(Map<String, dynamic> json) =>
      _$MangaHistoryFromJson(json);
}
