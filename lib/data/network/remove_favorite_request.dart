import 'package:json_annotation/json_annotation.dart';

part 'remove_favorite_request.g.dart';

@JsonSerializable()
class RemoveFavoriteRequest {
  late String manga;

  RemoveFavoriteRequest({required this.manga});

  factory RemoveFavoriteRequest.fromJson(Map<String, dynamic> json) =>
      _$RemoveFavoriteRequestFromJson(json);

  Map<String, dynamic> toJson() => _$RemoveFavoriteRequestToJson(this);
}
