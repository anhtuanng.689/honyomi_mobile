import 'package:json_annotation/json_annotation.dart';

part 'paging_meta.g.dart';

@JsonSerializable()
class PagingMeta {
  PagingMeta({
    required this.currentPage,
    required this.perPage,
    required this.totalItems,
    required this.totalPages,
  });

  late int currentPage;
  late int perPage;
  late int totalItems;
  late int totalPages;

  factory PagingMeta.fromJson(Map<String, dynamic> json) =>
      _$PagingMetaFromJson(json);

  Map<String, dynamic> toJson() => _$PagingMetaToJson(this);
}
