// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_favorite_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RemoveFavoriteRequest _$RemoveFavoriteRequestFromJson(
        Map<String, dynamic> json) =>
    RemoveFavoriteRequest(
      manga: json['manga'] as String,
    );

Map<String, dynamic> _$RemoveFavoriteRequestToJson(
        RemoveFavoriteRequest instance) =>
    <String, dynamic>{
      'manga': instance.manga,
    };
