// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paging_meta.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PagingMeta _$PagingMetaFromJson(Map<String, dynamic> json) => PagingMeta(
      currentPage: json['currentPage'] as int,
      perPage: json['perPage'] as int,
      totalItems: json['totalItems'] as int,
      totalPages: json['totalPages'] as int,
    );

Map<String, dynamic> _$PagingMetaToJson(PagingMeta instance) =>
    <String, dynamic>{
      'currentPage': instance.currentPage,
      'perPage': instance.perPage,
      'totalItems': instance.totalItems,
      'totalPages': instance.totalPages,
    };
