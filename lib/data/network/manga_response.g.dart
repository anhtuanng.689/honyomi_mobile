// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'manga_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MangaResponse _$MangaResponseFromJson(Map<String, dynamic> json) =>
    MangaResponse(
      data: Manga.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MangaResponseToJson(MangaResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
