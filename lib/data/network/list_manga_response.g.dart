// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_manga_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListMangaResponse _$ListMangaResponseFromJson(Map<String, dynamic> json) =>
    ListMangaResponse(
      data: (json['data'] as List<dynamic>)
          .map((e) => Manga.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ListMangaResponseToJson(ListMangaResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
