// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_chapter_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LatestMangaResponse _$LatestMangaResponseFromJson(Map<String, dynamic> json) =>
    LatestMangaResponse(
      data: (json['data'] as List<dynamic>)
          .map((e) => LatestChapter.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LatestMangaResponseToJson(
        LatestMangaResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
