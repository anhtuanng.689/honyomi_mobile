import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/network/paging_meta.dart';
import 'package:json_annotation/json_annotation.dart';

part 'new_series_manga.g.dart';

@JsonSerializable()
class NewSeriesManga {
  late List<Manga> data;
  late PagingMeta meta;

  NewSeriesManga(this.data, this.meta);

  factory NewSeriesManga.fromJson(Map<String, dynamic> json) =>
      _$NewSeriesMangaFromJson(json);

  Map<String, dynamic> toJson() => _$NewSeriesMangaToJson(this);
}
