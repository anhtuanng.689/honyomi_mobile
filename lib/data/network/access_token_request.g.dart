// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'access_token_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccessTokenRequest _$AccessTokenRequestFromJson(Map<String, dynamic> json) =>
    AccessTokenRequest(
      refreshToken: json['refreshToken'] as String,
    );

Map<String, dynamic> _$AccessTokenRequestToJson(AccessTokenRequest instance) =>
    <String, dynamic>{
      'refreshToken': instance.refreshToken,
    };
