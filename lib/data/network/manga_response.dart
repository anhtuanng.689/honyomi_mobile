import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:json_annotation/json_annotation.dart';

part 'manga_response.g.dart';

@JsonSerializable()
class MangaResponse {
  late Manga data;

  MangaResponse({required this.data});

  factory MangaResponse.fromJson(Map<String, dynamic> json) =>
      _$MangaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MangaResponseToJson(this);
}

