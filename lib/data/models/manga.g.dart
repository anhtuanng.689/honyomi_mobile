// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'manga.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Manga _$MangaFromJson(Map<String, dynamic> json) => Manga(
      id: json['id'] as String,
      title: json['title'] as String,
      mangaType: json['mangaType'] as String,
      romajiTitle: json['romajiTitle'] as String,
      status: $enumDecode(_$StatusEnumMap, json['status']),
      categories: (json['categories'] as List<dynamic>?)
          ?.map((e) => Category.fromJson(e as Map<String, dynamic>))
          .toList(),
      synopsis: json['synopsis'] as String,
      ageRating: json['ageRating'] as String,
      coverImage:
          CoverImage.fromJson(json['coverImage'] as Map<String, dynamic>),
      createdAt: json['createdAt'] as String?,
      authors: (json['authors'] as List<dynamic>?)
          ?.map((e) => Authors.fromJson(e))
          .toList(),
      chapters: (json['chapters'] as List<dynamic>?)
          ?.map((e) => Chapter.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MangaToJson(Manga instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'mangaType': instance.mangaType,
      'romajiTitle': instance.romajiTitle,
      'status': _$StatusEnumMap[instance.status],
      'categories': instance.categories,
      'synopsis': instance.synopsis,
      'ageRating': instance.ageRating,
      'coverImage': instance.coverImage,
      'createdAt': instance.createdAt,
      'authors': instance.authors,
      'chapters': instance.chapters,
    };

const _$StatusEnumMap = {
  Status.ongoing: 'ongoing',
  Status.finished: 'finished',
  Status.dropped: 'dropped',
};

Category _$CategoryFromJson(Map<String, dynamic> json) => Category(
      nsfw: json['nsfw'] as bool,
      description: json['description'] as String,
      title: json['title'] as String,
      id: json['id'] as String,
    );

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'nsfw': instance.nsfw,
      'description': instance.description,
      'title': instance.title,
      'id': instance.id,
    };

Authors _$AuthorsFromJson(Map<String, dynamic> json) => Authors(
      description: json['description'] as String,
      name: json['name'] as String,
      id: json['id'] as String,
    );

Map<String, dynamic> _$AuthorsToJson(Authors instance) => <String, dynamic>{
      'description': instance.description,
      'name': instance.name,
      'id': instance.id,
    };
