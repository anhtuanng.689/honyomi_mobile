// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cover_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CoverImage _$CoverImageFromJson(Map<String, dynamic> json) => CoverImage(
      original: Original.fromJson(json['original'] as Map<String, dynamic>),
      id: json['id'] as String,
    );

Map<String, dynamic> _$CoverImageToJson(CoverImage instance) =>
    <String, dynamic>{
      'original': instance.original,
      'id': instance.id,
    };

Original _$OriginalFromJson(Map<String, dynamic> json) => Original(
      heightInPx: json['heightInPx'] as int,
      widthInPx: json['widthInPx'] as int,
      url: json['url'] as String,
      id: json['id'] as String,
    );

Map<String, dynamic> _$OriginalToJson(Original instance) => <String, dynamic>{
      'heightInPx': instance.heightInPx,
      'widthInPx': instance.widthInPx,
      'url': instance.url,
      'id': instance.id,
    };
