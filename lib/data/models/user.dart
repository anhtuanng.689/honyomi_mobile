import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  late String id;
  late String avatarUrl;
  late String role;
  late String email;
  late String username;
  late Setting setting;
  User(
      {required this.id,
      required this.username,
      required this.email,
      required this.role,
      required this.avatarUrl,
      required this.setting});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

@JsonSerializable()
class Setting {
  late String language;

  Setting({required this.language});

  factory Setting.fromJson(Map<String, dynamic> json) =>
      _$SettingFromJson(json);
}
