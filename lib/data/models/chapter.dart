import 'package:json_annotation/json_annotation.dart';

part 'chapter.g.dart';

@JsonSerializable()
class Chapter {
  late String id;
  late int? viewCount;
  late String number;
  late String thumbnailUrl;
  late List<String> pages;
  late String translatedLanguage;
  late String? originalLanguage;
  late String title;

  Chapter({
    required this.id,
    required this.viewCount,
    required this.number,
    required this.thumbnailUrl,
    required this.pages,
    required this.translatedLanguage,
    required this.originalLanguage,
    required this.title,
  });

  factory Chapter.fromJson(Map<String, dynamic> json) =>
      _$ChapterFromJson(json);

  Map<String, dynamic> toJson() => _$ChapterToJson(this);
}
