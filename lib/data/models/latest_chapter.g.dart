// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_chapter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LatestChapter _$LatestChapterFromJson(Map<String, dynamic> json) =>
    LatestChapter(
      id: json['id'] as String,
      title: json['title'] as String,
      translatedLanguage: json['translatedLanguage'] as String,
      pages: (json['pages'] as List<dynamic>).map((e) => e as String).toList(),
      thumbnailUrl: json['thumbnailUrl'] as String,
      manga: MangaInLatest.fromJson(json['manga'] as Map<String, dynamic>),
      number: json['number'] as String,
    );

Map<String, dynamic> _$LatestChapterToJson(LatestChapter instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'translatedLanguage': instance.translatedLanguage,
      'pages': instance.pages,
      'thumbnailUrl': instance.thumbnailUrl,
      'manga': instance.manga,
      'number': instance.number,
    };

MangaInLatest _$MangaInLatestFromJson(Map<String, dynamic> json) =>
    MangaInLatest(
      id: json['id'] as String,
      title: json['title'] as String,
      coverImage:
          CoverImage.fromJson(json['coverImage'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MangaInLatestToJson(MangaInLatest instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'coverImage': instance.coverImage,
    };
