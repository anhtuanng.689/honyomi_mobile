import 'package:json_annotation/json_annotation.dart';

import 'cover_image.dart';

part 'latest_chapter.g.dart';

@JsonSerializable()
class LatestChapter {
  late String id;
  late String title;
  late String translatedLanguage;
  late List<String> pages;
  late String thumbnailUrl;
  late MangaInLatest manga;
  late String number;

  LatestChapter(
      {required this.id,
      required this.title,
      required this.translatedLanguage,
      required this.pages,
      required this.thumbnailUrl,
      required this.manga,
      required this.number});

  factory LatestChapter.fromJson(Map<String, dynamic> json) =>
      _$LatestChapterFromJson(json);

  Map<String, dynamic> toJson() => _$LatestChapterToJson(this);
}

@JsonSerializable()
class MangaInLatest {
  late String id;
  late String title;
  late CoverImage coverImage;

  MangaInLatest(
      {required this.id, required this.title, required this.coverImage});

  factory MangaInLatest.fromJson(Map<String, dynamic> json) =>
      _$MangaInLatestFromJson(json);

  Map<String, dynamic> toJson() => _$MangaInLatestToJson(this);
}
