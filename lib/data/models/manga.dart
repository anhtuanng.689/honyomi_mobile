import 'package:honyomi_mobile/data/models/chapter.dart';
import 'package:honyomi_mobile/data/models/cover_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'manga.g.dart';

@JsonSerializable()
class Manga {
  late String id;
  late String title;
  late String mangaType;
  late String romajiTitle;
  late Status status;
  late List<Category>? categories;
  late String synopsis;
  late String ageRating;
  late CoverImage coverImage;
  late String? createdAt;
  late List<Authors>? authors;
  late List<Chapter>? chapters;

  Manga(
      {required this.id,
      required this.title,
      required this.mangaType,
      required this.romajiTitle,
      required this.status,
      required this.categories,
      required this.synopsis,
      required this.ageRating,
      required this.coverImage,
      this.createdAt,
      this.authors,
      this.chapters});

  factory Manga.fromJson(Map<String, dynamic> json) => _$MangaFromJson(json);

  Map<String, dynamic> toJson() => _$MangaToJson(this);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Manga && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}

@JsonSerializable()
class Category {
  late bool nsfw;
  late String description;
  late String title;
  late String id;

  Category(
      {required this.nsfw,
      required this.description,
      required this.title,
      required this.id});

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}

@JsonSerializable()
class Authors {
  Authors({
    required this.description,
    required this.name,
    required this.id,
  });

  late String description;
  late String name;
  late String id;

  factory Authors.fromJson(dynamic json) => _$AuthorsFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorsToJson(this);
}

enum Status {
  @JsonValue("ongoing")
  ongoing,
  @JsonValue("finished")
  finished,
  @JsonValue("dropped")
  dropped
}
