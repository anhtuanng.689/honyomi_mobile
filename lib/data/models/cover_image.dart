import 'package:json_annotation/json_annotation.dart';

part 'cover_image.g.dart';

@JsonSerializable()
class CoverImage {
  late Original original;
  late String id;

  CoverImage({required this.original, required this.id});

  factory CoverImage.fromJson(Map<String, dynamic> json) =>
      _$CoverImageFromJson(json);

  Map<String, dynamic> toJson() => _$CoverImageToJson(this);
}

@JsonSerializable()
class Original {
  late int heightInPx;
  late int widthInPx;
  late String url;
  late String id;

  Original(
      {required this.heightInPx,
      required this.widthInPx,
      required this.url,
      required this.id});

  factory Original.fromJson(Map<String, dynamic> json) =>
      _$OriginalFromJson(json);

  Map<String, dynamic> toJson() => _$OriginalToJson(this);
}
