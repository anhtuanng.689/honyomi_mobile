// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: json['id'] as String,
      username: json['username'] as String,
      email: json['email'] as String,
      role: json['role'] as String,
      avatarUrl: json['avatarUrl'] as String,
      setting: Setting.fromJson(json['setting'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'avatarUrl': instance.avatarUrl,
      'role': instance.role,
      'email': instance.email,
      'username': instance.username,
      'setting': instance.setting,
    };

Setting _$SettingFromJson(Map<String, dynamic> json) => Setting(
      language: json['language'] as String,
    );

Map<String, dynamic> _$SettingToJson(Setting instance) => <String, dynamic>{
      'language': instance.language,
    };
