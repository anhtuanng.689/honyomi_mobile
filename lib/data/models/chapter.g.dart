// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chapter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Chapter _$ChapterFromJson(Map<String, dynamic> json) => Chapter(
      id: json['id'] as String,
      viewCount: json['viewCount'] as int?,
      number: json['number'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
      pages: (json['pages'] as List<dynamic>).map((e) => e as String).toList(),
      translatedLanguage: json['translatedLanguage'] as String,
      originalLanguage: json['originalLanguage'] as String?,
      title: json['title'] as String,
    );

Map<String, dynamic> _$ChapterToJson(Chapter instance) => <String, dynamic>{
      'id': instance.id,
      'viewCount': instance.viewCount,
      'number': instance.number,
      'thumbnailUrl': instance.thumbnailUrl,
      'pages': instance.pages,
      'translatedLanguage': instance.translatedLanguage,
      'originalLanguage': instance.originalLanguage,
      'title': instance.title,
    };
