import 'package:dio/dio.dart';

class BearerTokenInterceptor extends Interceptor {
  late String accessToken;

  BearerTokenInterceptor({required this.accessToken});

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers["Authorization"] = "Bearer $accessToken";
    super.onRequest(options, handler);
  }
}
