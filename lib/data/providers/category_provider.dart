import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/network/categories_response.dart';
import 'package:honyomi_mobile/public/constant.dart';

class CategoryProvider extends GetConnect {
  Future<List<Category>> getAllCategories() async {
    final response = await get("$baseCategoryApi/");

    if (response.hasError) {
      return Future.error(response.status);
    }

    return CategoriesResponse.fromJson(response.body).data;
  }
}
