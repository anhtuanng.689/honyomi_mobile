import 'package:flutter/foundation.dart' as foundation;
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/latest_chapter.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/network/list_manga_response.dart';
import 'package:honyomi_mobile/data/network/latest_chapter_response.dart';
import 'package:honyomi_mobile/data/network/manga_response.dart';
import 'package:honyomi_mobile/data/network/new_series_manga.dart';

import '../../public/constant.dart';

class MangaProvider extends GetConnect {
  Future<List<LatestChapter>?> getLatestChapter(language, limit) async {
    final response =
        await get("$baseMangaApi/chapters/latest?lang=$language&limit=$limit");
    if (response.status.hasError) {
      return Future.error(response.statusCode!);
    } else {
      return LatestMangaResponse.fromJson(response.body).data;
    }
  }

  Future<List<Manga>?> getNewSeries() async {
    final response = await get(
        "$baseMangaApi?filter[status]=ongoing&include[]=authors&include[]=categories");
    if (response.status.hasError) {
      return Future.error(response.statusCode!);
    } else {
      return NewSeriesManga.fromJson(response.body).data;
    }
  }

  Future<Manga> getMangaDetail(id) async {
    final response = await get("$baseMangaApi/$id");
    if (response.status.hasError) {
      return Future.error(response.statusCode!);
    } else {
      return MangaResponse.fromJson(response.body).data;
    }
  }

  Future<List<Manga>> getManga(
      {List<Category>? categories, List<Status>? status, String? query}) async {
    final queryCategories = categories?.toQuery() ?? "";
    final queryStatus = status?.toQuery() ?? "";
    final queryString = query?.toQuery() ?? "";

    final response = await get(
        "$baseMangaApi?include[]=authors&include[]=categories$queryCategories$queryStatus$queryString");
    if (response.status.hasError) {
      return Future.error(response.statusCode!);
    } else {
      return ListMangaResponse.fromJson(response.body).data;
    }
  }
}

extension CategoryQuery on List<Category> {
  String toQuery() {
    if (isEmpty) {
      return "";
    }
    var buffer = StringBuffer("");

    forEach((category) {
      buffer.write("&filter[categories][]=${category.id}");
    });

    return buffer.toString();
  }
}

extension StatusQuery on List<Status> {
  String toQuery() {
    if (isEmpty) {
      return "";
    }

    var buffer = StringBuffer("");

    forEach((status) {
      buffer.write("&filter[statuses][]=${foundation.describeEnum(status)}");
    });

    return buffer.toString();
  }
}

extension StringQuery on String {
  String toQuery() {
    if (isEmpty) {
      return "";
    }

    var buffer = StringBuffer("");

    buffer.write("&q=$this");

    return buffer.toString();
  }
}
