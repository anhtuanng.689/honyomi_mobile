import 'package:dio/dio.dart';
import 'package:honyomi_mobile/data/providers/interceptors/authentication_response_interceptor.dart';
import 'package:honyomi_mobile/data/providers/interceptors/bearer_token_interceptor.dart';

class DioProvider {
  final _dio = Dio();

  Dio getDio({
    bool handleAuth = false,
    String? accessToken,
  }) {
    if (handleAuth) {
      _dio.interceptors.add(AuthenticationResponseInterceptorHandler());
    }

    if (accessToken != null) {
      _dio.interceptors.add(BearerTokenInterceptor(accessToken: accessToken));
    }

    return _dio;
  }
}
