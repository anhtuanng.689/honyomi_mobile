import 'package:dio/dio.dart';
import 'package:honyomi_mobile/data/network/login_request.dart';
import 'package:honyomi_mobile/data/network/login_response.dart';
import 'package:honyomi_mobile/data/providers/dio_provider.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:honyomi_mobile/data/network/access_token_request.dart';
import 'package:honyomi_mobile/data/network/access_token_response.dart';

class AuthProvider {
  final dio = DioProvider().getDio(handleAuth: true);

  Future<Tokens> signIn(LoginRequest loginRequest) async {
    try {
      Response response =
          await dio.post("$baseAuthApi/login", data: loginRequest.toJson());

      return LoginResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<String> getAccessToken(AccessTokenRequest accessTokenRequest) async {
    try {
      Response response =
          await dio.post("$baseAuthApi/access", data: accessTokenRequest);

      return AccessTokenResponse.fromJson(response.data).data.accessToken;
    } catch (e) {
      rethrow;
    }
  }
}
