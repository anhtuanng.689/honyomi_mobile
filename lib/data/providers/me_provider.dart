import 'package:dio/dio.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/data/models/user.dart';
import 'package:honyomi_mobile/data/network/add_favorite_request.dart';
import 'package:honyomi_mobile/data/network/list_manga_response.dart';
import 'package:honyomi_mobile/data/network/me/history_response.dart';
import 'package:honyomi_mobile/data/network/user_response.dart';
import 'package:honyomi_mobile/public/constant.dart';

import 'dio_provider.dart';

class MeProvider {
  late String accessToken;
  late Dio dio;

  MeProvider({required this.accessToken}) {
    dio = DioProvider().getDio(handleAuth: true, accessToken: accessToken);
  }

  Future<User> getProfile() async {
    try {
      Response response = await dio.get("$baseMeApi/profile");
      return UserResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Manga>> getFavorites() async {
    try {
      Response response = await dio.get("$baseMeApi/favorite");
      return ListMangaResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<History>> getHistory() async {
    try {
      Response response = await dio.get("$baseMeApi/history/chapters");
      return HistoryResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Manga>> addFavorites(String id) async {
    try {
      Response response = await dio.post("$baseMeApi/favorite",
          data: AddFavoriteRequest(manga: id));
      return ListMangaResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Manga>> deleteFavorites(String id) async {
    try {
      Response response = await dio.delete("$baseMeApi/favorite",
          data: AddFavoriteRequest(manga: id));
      return ListMangaResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }
}
