import 'package:dio/dio.dart';
import 'package:honyomi_mobile/data/models/user.dart';
import 'package:honyomi_mobile/data/network/user_response.dart';
import 'package:honyomi_mobile/data/providers/dio_provider.dart';
import 'package:honyomi_mobile/public/constant.dart';

class UserProvider {
  late String accessToken;
  late Dio dio;

  UserProvider({required this.accessToken}) {
    dio = DioProvider().getDio(handleAuth: true, accessToken: accessToken);
  }

  Future<User> getProfile() async {
    try {
      Response response = await dio.get("$baseMeApi/profile");
      return UserResponse.fromJson(response.data).data;
    } catch (e) {
      rethrow;
    }
  }
}
