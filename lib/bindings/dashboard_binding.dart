import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:honyomi_mobile/controllers/dashboard_controller.dart';
import 'package:honyomi_mobile/controllers/discover/discover_controller.dart';
import 'package:honyomi_mobile/controllers/home/home_controller.dart';
import 'package:honyomi_mobile/controllers/user/user_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(() => DashboardController());
    Get.put(() => HomeController());
    Get.lazyPut(() => DiscoverController());
    Get.lazyPut(() => UserController());
  }
}
