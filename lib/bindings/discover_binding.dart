import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:honyomi_mobile/controllers/discover/discover_controller.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class DiscoverBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MangaProvider());
    Get.lazyPut(() => DiscoverController());
  }
}
