import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:honyomi_mobile/controllers/detail/detail_controller.dart';
import 'package:honyomi_mobile/controllers/library/favorite_controller.dart';

class DetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetailController());
    Get.lazyPut(() => FavoriteController());
  }
}
