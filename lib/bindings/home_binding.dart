import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/controllers/home/latest_manga_controller.dart';
import 'package:honyomi_mobile/controllers/home/new_series_controller.dart';
import 'package:honyomi_mobile/controllers/dashboard_controller.dart';
import 'package:honyomi_mobile/controllers/home/recent_controller.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(() => MangaProvider());
    Get.lazyPut(() => LatestMangaController());
    Get.lazyPut(() => NewSeriesController());
    Get.lazyPut(() => DashboardController());
    Get.lazyPut(() => AuthenticationController());
    Get.lazyPut(() => RecentController());
  }
}
