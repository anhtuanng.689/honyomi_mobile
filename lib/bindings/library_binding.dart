import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:honyomi_mobile/controllers/library/favorite_controller.dart';
import 'package:honyomi_mobile/controllers/library/library_controller.dart';
import 'package:honyomi_mobile/data/providers/manga_provider.dart';

class LibraryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MangaProvider());
    Get.lazyPut(() => LibraryController());
    Get.lazyPut(() => FavoriteController());
  }
}
