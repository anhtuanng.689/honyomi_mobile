import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:honyomi_mobile/controllers/auth/authentication_controller.dart';
import 'package:honyomi_mobile/controllers/user/user_controller.dart';

class SignInBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AuthenticationController());
    Get.lazyPut(() => UserController());
  }
}
