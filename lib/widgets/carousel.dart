import 'package:flutter/material.dart';
import 'package:getwidget/components/carousel/gf_carousel.dart';
import 'package:honyomi_mobile/public/constant.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

final List<String> imgList = [
  'https://mangapk1.com/?v=0&data=mKWkoaNrX2CjZF6ekZ%2BXkqCcXpSfnl9gkaSjlqSkX6WdoV%2BUmJKgpZWjj5qdkpeWX2lhY2VmaF5hX5qhl3CmbmFnY2JgZWBmZGk%3D',
  'https://r.r10s.jp/ran/img/2001/0009/784/065/240/410/20010009784065240410_1.jpg',
  'https://ecdn.game4v.com/g4v-content/uploads/2021/05/Komi-san-1-game4v.png',
  'https://i.pinimg.com/736x/6f/05/3e/6f053e5b8215874cb8eb5b3efb8a4a21.jpg',
  'https://images.catmanga.org/series/gksfd/covers/07.jpg',
];

class Carousel extends StatelessWidget {
  const Carousel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GFCarousel(
      aspectRatio: 1.05,
      viewportFraction: 1.0,
      pagerSize: 10,
      items: imgList.map(
        (url) {
          return GestureDetector(
            // onTap: () => Get.to(() => MangaDetail()),
            child: SizedBox(
              width: width,
              child: ImageLoader.loadDefault(url: url, fit: BoxFit.fill),
            ),
          );
        },
      ).toList(),
      autoPlay: true,
      pagination: true,
      activeIndicator: Colors.black,
      passiveIndicator: Colors.black54,
      onPageChanged: (index) {
        index;
      },
    );
  }
}
