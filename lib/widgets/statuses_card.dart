import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/discover/discover_controller.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/public/colors.dart';

class StatusesCard extends StatelessWidget {
  StatusesCard({
    Key? key,
    required this.statuses,
    required this.title,
  }) : super(key: key);

  final List<Status> statuses;
  final String title;

  final DiscoverController discoverController = Get.put(DiscoverController());

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding:
            title == "detail" ? EdgeInsets.only(top: 10.h) : EdgeInsets.zero,
        child: Row(
          children: statuses
              .map(
                (e) => Padding(
                  padding: EdgeInsets.only(right: 5.w),
                  child: GestureDetector(
                    onTap: () {
                      print(e);
                      if (discoverController.statuses.contains(e)) {
                        discoverController.statuses.remove(e);
                      } else {
                        discoverController.statuses.add(e);
                      }
                      discoverController.handleFilter();
                    },
                    child: ChoiceChip(
                      selectedColor: Colors.grey.shade500,
                      label: Text(
                        describeEnum(e),
                        style: TextStyle(
                            color:
                                Theme.of(context).brightness == Brightness.dark
                                    ? Colors.white
                                    : Colors.black),
                      ),
                      selected: discoverController.statuses.contains(e),
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
