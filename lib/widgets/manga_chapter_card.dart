import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/latest_chapter.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

class MangaChapterCard extends StatelessWidget {
  const MangaChapterCard({Key? key, required this.latestChapter})
      : super(key: key);
  final LatestChapter latestChapter;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {Get.toNamed("/detail/${latestChapter.manga.id}")},
      child: SizedBox(
        width: 0.6.sw,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.r),
                    child: ImageLoader.loadDefault(
                        url: latestChapter.thumbnailUrl)),
              ),
            ),
            SizedBox(
              height: 5.h,
            ),
            Text(
              latestChapter.manga.title,
              maxLines: 2,
              style: TextStyle(
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500,
                  overflow: TextOverflow.ellipsis),
            ),
            SizedBox(
              height: 5.h,
            ),
            Text(
              'Chapter ${latestChapter.number}',
              style: TextStyle(
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w400,
                  overflow: TextOverflow.ellipsis),
            ),
          ],
        ),
      ),
    );
  }
}
