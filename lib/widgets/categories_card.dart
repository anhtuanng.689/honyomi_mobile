import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/controllers/discover/discover_controller.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/public/colors.dart';

class CategoriesCard extends StatelessWidget {
  CategoriesCard({Key? key, required this.categories, required this.title})
      : super(key: key);

  final List<Category> categories;
  final String title;

  final DiscoverController discoverController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding:
            title == "detail" ? EdgeInsets.only(top: 10.h) : EdgeInsets.zero,
        child: Row(
          children: categories
              .map(
                (e) => Padding(
                    padding: EdgeInsets.only(right: 5.w),
                    child: title == "detail"
                        ? buildCategoryCard(context, e, false, () {})
                        : buildCategoryCard(context, e,
                            discoverController.categories.contains(e), () {
                            if (discoverController.categories.contains(e)) {
                              discoverController.categories.remove(e);
                            } else {
                              discoverController.categories.add(e);
                            }
                            discoverController.handleFilter();
                          })),
              )
              .toList(),
        ),
      ),
    );
  }

  Widget buildCategoryCard(BuildContext context, Category category,
      bool selected, VoidCallback callback) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: ChoiceChip(
        selectedColor: Colors.grey.shade500,
        label: Text(
          category.title,
          style: TextStyle(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.white
                  : Colors.black),
        ),
        selected: selected,
      ),
    );
  }
}
