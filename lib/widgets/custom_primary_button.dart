import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomPrimaryButton extends StatelessWidget {
  final double height;
  final String textValue;
  final Color? buttonColor;
  final Color? textColor;
  final IconData? icon;
  final VoidCallback onTap;

  const CustomPrimaryButton(
      {Key? key,
      required this.height,
      required this.textValue,
      this.buttonColor,
      this.textColor,
      required this.onTap,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(15),
      elevation: 0,
      child: Container(
        height: height,
        decoration: BoxDecoration(
          color: buttonColor,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              onTap();
            },
            borderRadius: BorderRadius.circular(15),
            child: icon != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 20.w,
                      ),
                      Icon(
                        icon,
                        size: 25.w,
                      ),
                      SizedBox(
                        width: 20.w,
                      ),
                      Text(
                        textValue,
                        style: TextStyle(
                          fontSize: 18.sp,
                          fontWeight: FontWeight.w600,
                        ).copyWith(color: textColor),
                      ),
                      const Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 25.w,
                      ),
                      SizedBox(
                        width: 20.w,
                      ),
                    ],
                  )
                : Center(
                    child: Text(
                      textValue,
                      style: TextStyle(
                        fontSize: 18.sp,
                        fontWeight: FontWeight.w600,
                      ).copyWith(color: textColor),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
