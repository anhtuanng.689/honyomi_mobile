import 'package:flutter/material.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

class ChapterReader extends StatelessWidget {
  const ChapterReader({Key? key, required this.listChapterUrl})
      : super(key: key);
  final List<String> listChapterUrl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          shrinkWrap: true,
          itemCount: listChapterUrl.length,
          itemBuilder: (context, index) =>
              ImageLoader.loadDefault(url: listChapterUrl[index])),
    );
  }
}
