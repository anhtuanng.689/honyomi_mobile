import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:honyomi_mobile/controllers/dashboard_controller.dart';
import 'package:honyomi_mobile/public/colors.dart';

class Navbar extends StatelessWidget {
  Navbar({Key? key}) : super(key: key);

  final DashboardController dashboardController =
      Get.put(DashboardController());

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).brightness == Brightness.dark
            ? ColorConstants.appBarDarkColor
            : Colors.white,
        border: Border(
          top: BorderSide(width: .1.w),
        ),
        boxShadow: [
          BoxShadow(
            blurRadius: 4,
            color: Colors.grey.withOpacity(.35),
          )
        ],
      ),
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.0.w, vertical: 5.h),
          child: GNav(
            rippleColor: Colors.grey[400]!,
            hoverColor: Colors.grey[400]!,
            gap: 10.w,
            // activeColor: Colors.black,
            iconSize: 24.sp,
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
            duration: const Duration(milliseconds: 600),
            // tabBackgroundColor: Colors.grey[100]!,
            color: Colors.black,
            tabs: [
              GButton(
                icon: Icons.home,
                text: '',
                iconColor: Colors.grey.shade500,
              ),
              GButton(
                icon: Icons.search,
                text: '',
                iconColor: Colors.grey.shade500,
              ),
              GButton(
                icon: Icons.military_tech,
                text: '',
                iconColor: Colors.grey.shade500,
              ),
              GButton(
                icon: Icons.person,
                text: '',
                iconColor: Colors.grey.shade500,
              ),
            ],
            selectedIndex: dashboardController.tabIndex.value,
            onTabChange: (index) {
              print("press $index");
              dashboardController.changeTabIndex(index);
            },
          ),
        ),
      ),
    );
  }
}
