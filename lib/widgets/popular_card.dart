import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/public/colors.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

class PopularCard extends StatelessWidget {
  const PopularCard({Key? key, required this.manga, required this.index})
      : super(key: key);

  final Manga manga;
  final int index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {Get.toNamed("/detail/${manga.id}")},
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.r),
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: index == 1
                      ? [
                          Colors.orangeAccent.shade100,
                          Colors.orange,
                        ]
                      : index == 2
                          ? [Colors.lightBlueAccent.shade100, Colors.blue]
                          : index == 3
                              ? [Colors.lightGreenAccent.shade100, Colors.green]
                              : Theme.of(context).brightness == Brightness.dark
                                  ? [Colors.grey.shade800, Colors.grey.shade900]
                                  : [Colors.grey.shade50, Colors.grey.shade400],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: Row(
              children: [
                SizedBox(
                  width: 0.075.sw,
                  child: index == 1 || index == 2 || index == 3
                      ? const Center(child: Icon(Icons.star))
                      : Center(
                          child: Text(
                            index.toString(),
                            style: TextStyle(
                                fontSize: 20.sp,
                                fontWeight: FontWeight.w500,
                                overflow: TextOverflow.ellipsis),
                          ),
                        ),
                ),
                SizedBox(
                  width: 10.w,
                ),
                Flexible(
                  flex: 1,
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: ImageLoader.loadDefault(
                      url: manga.coverImage.original.url,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.w,
                ),
                Flexible(
                  flex: 3,
                  child: Text(
                    manga.romajiTitle,
                    maxLines: 2,
                    style: TextStyle(
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w500,
                        overflow: TextOverflow.ellipsis),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
