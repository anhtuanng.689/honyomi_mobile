import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:honyomi_mobile/data/models/manga.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

class MangaCard extends StatelessWidget {
  const MangaCard({Key? key, required this.manga}) : super(key: key);
  final Manga manga;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {Get.toNamed("/detail/${manga.id}")},
      child: SizedBox(
        width: 0.6.sw,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.r),
                    child: ImageLoader.loadDefault(
                        url: manga.coverImage.original.url)),
              ),
            ),
            SizedBox(
              height: 5.h,
            ),
            Text(
              manga.title,
              maxLines: 2,
              style: TextStyle(
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500,
                  overflow: TextOverflow.ellipsis),
            ),
          ],
        ),
      ),
    );
  }
}
