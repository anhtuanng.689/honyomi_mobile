import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:honyomi_mobile/data/network/me/history_response.dart';
import 'package:honyomi_mobile/public/image_loader.dart';

class RecentMangaCard extends StatelessWidget {
  const RecentMangaCard({Key? key, required this.history}) : super(key: key);
  final History history;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: .25.w, color: Colors.grey.shade700),
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      width: 0.8.sw,
      child: Padding(
        padding: EdgeInsets.all(5.0.w),
        child: Row(
          children: [
            Flexible(
              flex: 4,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.r),
                  child: ImageLoader.loadDefault(
                      url: history.chapter.manga.coverImage.original.url,
                      fit: BoxFit.fill)),
            ),
            SizedBox(
              width: 10.w,
            ),
            Flexible(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    history.chapter.manga.title,
                    maxLines: 2,
                    style: TextStyle(
                        fontSize: 18.sp,
                        fontWeight: FontWeight.w500,
                        overflow: TextOverflow.ellipsis),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Text(
                    'Chapter ${history.chapter.number}',
                    style: TextStyle(
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
