import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:honyomi_mobile/data/models/chapter.dart';
import 'package:honyomi_mobile/public/colors.dart';

class ChapterCard extends StatelessWidget {
  const ChapterCard({Key? key, required this.chapter}) : super(key: key);
  final Chapter chapter;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.r),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        color: Theme.of(context).brightness == Brightness.dark
            ? ColorConstants.secondaryDarkAppColor
            : ColorConstants.lightScaffoldBackgroundColor,
        child: Padding(
          padding: EdgeInsets.all(15.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Chapter ${chapter.number}',
                style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w500,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(
                height: 10.sp,
              ),
              Text(
                chapter.title,
                style: TextStyle(
                    color: Theme.of(context).brightness == Brightness.dark
                        ? ColorConstants.tipColor
                        : Colors.grey.shade900,
                    overflow: TextOverflow.ellipsis),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
