import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SpinKit extends StatelessWidget {
  const SpinKit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buildSpinKit(context);
  }
}

Widget buildSpinKit(BuildContext context) {
  return SpinKitWave(
    size: 16.sp,
    color: Theme.of(context).brightness == Brightness.dark
        ? Colors.white
        : Colors.black,
  );
}
