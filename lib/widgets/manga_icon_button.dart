import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MangaIconButton extends StatelessWidget {
  const MangaIconButton({Key? key, required this.icon, this.isLeading = false})
      : super(key: key);
  final IconData icon;
  final bool isLeading;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: isLeading ? EdgeInsets.all(10.h) : const EdgeInsets.all(0),
      padding: isLeading ? const EdgeInsets.all(0) : EdgeInsets.all(5.h),
      decoration: BoxDecoration(
        color: Theme.of(context).brightness == Brightness.dark
            ? Colors.grey.shade800
            : Colors.white54,
        shape: BoxShape.circle,
      ),
      child: Icon(
        icon,
        size: 30.sp,
      ),
    );
  }
}
